// This file defines the ${service}-${environment}-http service


/********************* Load Balancer ********************/
// TODO(tom): Add a switch so that we can swap between a shared LB and a unique one
// https://www.terraform.io/docs/providers/aws/r/lb_listener_rule.html


locals {
    _security_groups = "sg-c90c6cb0 sg-d392deaa sg-4c3fad35"
    security_groups = "${split(" ", var.cfsg == true ? "${local._security_groups} sg-69abe710" : local._security_groups)}"
}

resource "aws_lb" "this" {
    name               = "${local.aws_lb_name}"
    internal           = false
    load_balancer_type = "application"

    enable_deletion_protection = "${var.deletion_protection}"

    subnets = ["subnet-31b56c47", "subnet-393f1e5e", "subnet-a7ee24fe"]
    security_groups = "${local.security_groups}"
}

resource "aws_lb_listener" "http" {
    load_balancer_arn = "${aws_lb.this.arn}"
    port              = "80"
    protocol          = "HTTP"

    default_action {
        type = "redirect"
        redirect {
            status_code = "HTTP_301"
            protocol = "HTTPS"
            port = "443"
        }
    }
}
resource "aws_lb_listener" "https" {
    load_balancer_arn = "${aws_lb.this.arn}"
    port              = "443"
    protocol          = "HTTPS"

    certificate_arn = "${aws_acm_certificate.cert.arn}"
    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.this.arn}"
    }
}

resource "aws_lb_target_group" "this" {
    name     = "${local.aws_lb_name}"
    port     = 80
    protocol = "HTTPS"
    vpc_id   = "${data.aws_vpc.this.id}"

    health_check {
        protocol = "HTTPS"
        path = "/status"
        timeout = 25
        matcher = "200"
    }
}

/********************* ECS Http Service ********************/
data "template_file" "http_task_definition" {
    template = "${file("task-definitions/http.json")}"

    vars {
        service = "${var.service}"
        name = "${var.name}"
        image = "${var.image}"
        deploy_time = "${timestamp()}"
    }
}

resource "aws_ecs_task_definition" "this" {
    family = "${var.service}-${var.name}-http"
    container_definitions = "${data.template_file.http_task_definition.rendered}"
    task_role_arn = "${aws_iam_role.this.arn}"
    network_mode = "bridge"


}

resource "aws_ecs_service" "this" {
    name            = "${var.service}-${var.name}-http"
    cluster         = "${data.aws_ecs_cluster.this.id}"
    task_definition = "${aws_ecs_task_definition.this.arn}"
    depends_on = ["aws_lb_listener.https", "aws_lb_listener.http", "aws_lb.this"]

    health_check_grace_period_seconds = 20

    desired_count = 1
    lifecycle {
        ignore_changes = ["desired_count"]
    }

    load_balancer {
        container_name = "php"
        container_port = 443
        target_group_arn = "${aws_lb_target_group.this.arn}"
    }

    ordered_placement_strategy {
        type  = "spread"
        field = "attribute:ecs.availability-zone"
    }
    ordered_placement_strategy {
        type  = "spread"
        field = "instanceId"
    }

    service_registries {
        container_name = "php"
        container_port = 443
        registry_arn = "${aws_service_discovery_service.this.arn}"
    }
}

resource "aws_service_discovery_service" "this" {
    name = "${var.service}-${var.name}-http"
    
    # Fix Service Discovery not defaulting to failure_threshold = 1
    health_check_custom_config {
        failure_threshold = 1
    }

    dns_config {
        dns_records {
            ttl  = 60
            type = "SRV"
        }

        namespace_id   = "${var.namespace}"
        routing_policy = "MULTIVALUE"
    }
}

/********************* Routes ********************/

locals {
    default_domains = "${list("${local.trimmed_name}.${var.service}.summer-abyss.com", "*.${local.trimmed_name}.${var.service}.summer-abyss.com")}"
    passed_domains = "${split(",", "${var.domains}")}"
    _domains = "${list(local.default_domains, local.passed_domains)}"
    domains = ["${local._domains[var.domains == "" ? 0 : 1]}"]

}

resource "aws_acm_certificate" "cert" {
    domain_name       = "${local.domains[0]}"
    subject_alternative_names = ["${slice(local.domains, 1, length(local.domains))}"]
    validation_method = "DNS"

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_acm_certificate_validation" "cert" {
    certificate_arn = "${aws_acm_certificate.cert.arn}"
    validation_record_fqdns = ["${aws_route53_record.cert_validation.*.fqdn}"]
}

resource "aws_route53_record" "cert_validation" {
    count = 1

    zone_id = "${data.aws_route53_zone.external.zone_id}"
    name = "${lookup(aws_acm_certificate.cert.domain_validation_options[count.index],"resource_record_name")}"
    type = "${lookup(aws_acm_certificate.cert.domain_validation_options[count.index], "resource_record_type")}"
    records = ["${lookup(aws_acm_certificate.cert.domain_validation_options[count.index], "resource_record_value")}"]
    ttl = 60
}


resource "aws_route53_record" "this" {
    count = "${length(local.domains)}"

    zone_id = "${data.aws_route53_zone.external.zone_id}"
    name = "${local.domains[count.index]}"
    type = "A"

    alias {
        name                   = "${aws_lb.this.dns_name}"
        zone_id                = "${aws_lb.this.zone_id}"
        evaluate_target_health = true
    }
}

output "domain" {
  value = "https://${element(aws_route53_record.this.*.fqdn, 0)}"
}

