provider "aws" {
  region = "ap-southeast-2"
  version = "v2.70.0"
}

terraform {
  backend "s3" {
    bucket = "agedcareguide-secrets"
    region = "ap-southeast-2"
  }
}

locals {
    trimmed_name_length = "${32 - (length(var.service)+1) - length("-http")}"
    trimmed_name = "${replace(trimspace(substr(replace(var.name, "-", " "), 0, min(local.trimmed_name_length, length(var.name)))), " ", "-")}"
    aws_lb_name = "${var.service}-${local.trimmed_name}-http"
}

data "aws_ecs_cluster" "this" {
    cluster_name = "acg-development"
}


data "aws_security_group" "cf" {
    id = "sg-69abe710"
}

resource "aws_cloudwatch_log_group" "this" {
    name = "${var.service}-${var.name}"
    retention_in_days = 7
}

### DNS and Routing

data "aws_vpc" "this" {
    id = "vpc-4f5bb12b"
}

data "aws_route53_zone" "external" {
    name = "${var.zone}"
}

data "aws_route53_zone" "internal" {
  name         = "local."
  private_zone = true
}
