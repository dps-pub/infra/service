
ifeq ($(OS),Windows_NT) 
    detected_OS := Windows
else
    detected_OS := $(shell sh -c 'uname 2>/dev/null | tr '[:upper:]' '[:lower:]' || echo Unknown')
endif

EXTRA_ARGS ?= 

IMAGE ?= 581680512126.dkr.ecr.ap-southeast-2.amazonaws.com/$(SERVICE):$(CI_COMMIT_SHA)

TERRAFORM_VERSION = 0.11.13
TERRAFORM = ./terraform_$(TERRAFORM_VERSION)

CHAMBER_VERSION = 2.3.2
CHAMBER = ./chamber_$(CHAMBER_VERSION)

test:
	$(MAKE) clean
	$(MAKE) plan.out
	$(MAKE) clean

update: plan;

clean: 
	rm -r .terraform || true
	rm plan.out || true
	rm backend.tfvars || true

check-env:
ifndef SERVICE
  $(error SERVICE is undefined)
endif
ifndef ENVIRONMENT
  $(error ENVIRONMENT is undefined)
endif
ifndef CI_COMMIT_SHA
  $(error CI_COMMIT_SHA is undefined)
endif
.PHONY: clean test check-env

.terraform: backend.tfvars $(TERRAFORM)
	@printf "\n\033[1;36mInitializing Terraform\033[0m\n"
	@$(TERRAFORM) init -verify-plugins=false -backend-config=$< .

plan.out: check-env .terraform $(CHAMBER) $(TERRAFORM) *.tf
	@printf "\n\033[1;36mChecking $(SERVICE)/$(ENVIRONMENT) env\033[0m\n"
	@$(CHAMBER) read $(SERVICE)/$(ENVIRONMENT) app_env || $(CHAMBER) export $(SERVICE)/develop | $(CHAMBER) import $(SERVICE)/$(ENVIRONMENT) -
	@printf "\n\033[1;36mPlanning Deployment\033[0m\n"
	@$(TERRAFORM) plan -out plan.out -var "service=$(SERVICE)" -var "name=$(ENVIRONMENT)" -var "image=$(IMAGE)" $(EXTRA_ARGS)

destroy: $(TERRAFORM) .terraform $(CREDENTIALS) *.tf
	@printf "\n\033[1;36mPlanning Destruction\033[0m\n"
	@$(TERRAFORM) plan -destroy -out plan.out -var "service=$(SERVICE)" -var "name=$(ENVIRONMENT)" -var "image=$(IMAGE)" $(EXTRA_ARGS)
	@printf "\n\033[1;36mDestroying Environment\033[0m\n"
	@$(TERRAFORM) apply plan.out
	@curl -s -X POST --data "{\"text\":\"Review Env of $(SERVICE) $(ENVIRONMENT) has been destroyed\"}" https://hooks.slack.com/services/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 > /dev/null

backend.tfvars:
	@echo "key=\"tf-state/service/$(SERVICE)/$(ENVIRONMENT)/terraform.tfstate\"" > backend.tfvars

$(CHAMBER):
	@printf "\033[1;36mDownloading Chamber v$(CHAMBER_VERSION)\033[0m\n"
	@curl -sL https://github.com/segmentio/chamber/releases/download/v$(CHAMBER_VERSION)/chamber-v$(CHAMBER_VERSION)-$(detected_OS)-amd64 -o $@
	@chmod +x $@

$(TERRAFORM):
	@printf "\033[1;36mDownloading Terraform v$(TERRAFORM_VERSION)\033[0m\n"
	@curl -sLO https://releases.hashicorp.com/terraform/$(TERRAFORM_VERSION)/terraform_$(TERRAFORM_VERSION)_$(detected_OS)_amd64.zip
	@unzip terraform_$(TERRAFORM_VERSION)_$(detected_OS)_amd64.zip > /dev/null
	@rm -f terraform_$(TERRAFORM_VERSION)_$(detected_OS)_amd64.zip;
	@chmod +x terraform;
	@mv terraform $@


define notify_body
{
	"attachments": [
		{
			"fallback": "$(GITLAB_USER_NAME) deployed a new commit to $(SERVICE) $(ENVIRONMENT)",
			"color": "#36a64f",
			"pretext": "$(GITLAB_USER_NAME) deployed a new commit to $(SERVICE) $(ENVIRONMENT)",
			"author_name": "$(CI_COMMIT_AUTHOR_NAME)",
			"title": "$(CI_COMMIT_TITLE)",
			"title_link": "$(CI_PROJECT_URL)/commit/$(CI_COMMIT_SHA)",
			"text": "$(CI_COMMIT_DESCRIPTION)",
			"footer": "Gitlab"
		}
	]
}
endef
export notify_body
apply: plan.out $(TERRAFORM) .terraform 
	@printf "\n\033[1;36mApplying Planned Deployment\033[0m\n"
	@$(TERRAFORM) apply $<
	@rm $<

ifdef ROLLBAR_SERVER_POST_KEY
	@printf "\033[1;36mNotifying Rollbar\033[0m\n"
	@curl -s https://api.rollbar.com/api/1/deploy/ \
		-F access_token=$(ROLLBAR_SERVER_POST_KEY) \
		-F environment=$(ENVIRONMENT) \
		-F revision=$(CI_COMMIT_SHA) \
		-F local_username=DPS > /dev/null
endif
ifeq ($(ENVIRONMENT),production)
	@printf "\033[1;36mNotifying #web-devs\033[0m\n"
	@curl -s -X POST -H 'Content-type: application/json' --data "$$notify_body" https://hooks.slack.com/services/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 > /dev/null
endif

plan: plan.out
ifeq ($(ENVIRONMENT),production)
	$(make) notify
endif
.PHONY: plan


define deploy_body
{
	"text": "$(SERVICE) $(ENVIRONMENT) is ready to promote to production <$(CI_PIPELINE_URL)|Here>",
}
endef
export deploy_body
notify:
	@printf "\033[1;36mAsking #web-devs to deploy\033[0m\n"
	@curl -s -X POST -H 'Content-type: application/json' --data "$$deploy_body" https://hooks.slack.com/services/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 > /dev/null
.PHONY: notify
