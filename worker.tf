
data "template_file" "worker_task_definition" {
    template = "${file("task-definitions/worker.json")}"

    vars {
        service = "${var.service}"
        name = "${var.name}"
        image = "${var.image}"
        deploy_time = "${timestamp()}"
    }
}

resource "aws_ecs_task_definition" "worker" {
    count = "${var.enable_extra_services ? 1 : 0}"

    family = "${var.service}-${var.name}-worker"
    container_definitions = "${data.template_file.worker_task_definition.rendered}"
    task_role_arn = "${aws_iam_role.this.arn}"
    network_mode = "bridge"
}

resource "aws_ecs_service" "worker" {
    count = "${var.enable_extra_services ? 1 : 0}"


    name            = "${var.service}-${var.name}-worker"
    cluster         = "${data.aws_ecs_cluster.this.id}"
    task_definition = "${aws_ecs_task_definition.worker.arn}"

    desired_count = 1
    lifecycle {
        ignore_changes = ["desired_count"]
    }


    ordered_placement_strategy {
        type  = "spread"
        field = "attribute:ecs.availability-zone"
    }
    ordered_placement_strategy {
        type  = "spread"
        field = "instanceId"
    }
}
