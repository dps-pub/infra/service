resource "aws_iam_role" "this" {
  name = "${var.service}-${var.name}-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
      Service = "${var.service}"
      Environment = "${var.name}"
  }
}

resource "aws_iam_role_policy" "ssmps" {
  name = "${var.service}-${var.name}-policy"
  role = "${aws_iam_role.this.id}"
    // Describe Key to decode 
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Action": [
            "kms:Decrypt",
            "ssm:GetParametersByPath"
        ],
        "Effect": "Allow",
        "Resource": [
            "arn:aws:ssm:ap-southeast-2:581680512126:parameter/${var.service}/${var.name}/*",
            "arn:aws:kms:ap-southeast-2:581680512126:key/630e8cd2-ecdf-46bf-aec5-45bfabfb8a62"
        ]
    }
  ]
}
EOF
}