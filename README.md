# Service

This is the basic deployment for all of our ECS Services

sample script
```sh
# Download the files from gitlab
$ curl -LO https://gitlab.com/dps-pub/infra/service/-/archive/master/service-master.zip
# Unzip and move them to current working directory
$ unzip service-master; mv service-master/* .; rmdir service-master; rm service-master.zip
# Save the backend state file location to backend.tfvars
$ echo "key=\"tf-state/service/$SERVICE/$ENVIRONMENT/terraform.tfstate\"" > backend.tfvars

# Init terraform with the s3 backend files
$ terraform init -backend-config=./backend.tfvars .
# Apply the changes or `terraform destroy` to take down the service
$ terraform apply -auto-approve -var "service=$SERVICE" -var "name=$ENVIRONMENT" -var "image=$IMAGE"
```

See [Service Test](https://gitlab.com/dps-pub/infra/service-test/) Repo for an example gitlab-ci.yml file
