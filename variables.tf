variable "name" {}

variable "service" {}

variable "domains" {
  default = ""
}

variable "zone" {
  default = "summer-abyss.com."
}


variable "namespace" {
  default = "ns-6zrndqejixleoqhi"
}

variable "image" {}

variable "deletion_protection" {
  default = false
}

variable "cfsg" {
  default = false
}

variable "enable_extra_services" {
  default = false
}

